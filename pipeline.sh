d=$(pwd)

#Download all the files specified in data/filenames
echo "Downloading files samples ..."

for url in $(cat $wd/data/urls)
do
    bash $wd/scripts/download.sh $url data yes
done

cd $wd
# Download the contaminants fasta file, and uncompress it
echo "Downloading fasta  file ..."
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes

cd $wd
# Index the contaminants file
echo "Runing STAR index contaminants ..."
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx

cd $wd
# Merge the samples into a single file
echo "Mergening the samples ..."
for sid in $(ls data/*.fastq.gz | cut -d"-" -f1 | sed 's:data/::' | sort | uniq)
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done

cd $wd
# TODO: run cutadapt for all merged files
mkdir -p out/trimmed
mkdir -p log/cutadapt

echo 'Running cutadapt for merged files...'
for sid in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sed 's:out/merged/::' | sort | uniq)
do
     cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sid}.trimmed.fastq.gz out/merged/${sid}.fastq.gz > log/cutadapt/${sid}.log
done

cd $wd
#TODO: run STAR for all trimmed files
echo "Running STAR for all merged files..."
for sid in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sed 's:out/merged/::' | sort | uniq)
do
    mkdir -p out/star/$sid
    STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/${sid}.trimmed.fastq.gz --readFilesCommand zcat --outFileNamePrefix out/star/${sid}/
done

# TODO: create a log file containing information from cutadapt and star logs
echo "Creating log file..."
for sid in $(ls out/merged/*.fastq.gz | cut -d "." -f1 | sed 's:out/merged/::' | sort | uniq)
do
    cat out/star/$sid/log.final.out | grep -e "Uniquely mapped reads %" -e "% of reads mapped to multiple loci" -e "% of reads mapped to too many loci" >> log/pipeline.log
    cat log/cutadapt/$sid.log | grep -e "Reads with adapters" -e "Total basepairs" >> log/pipeline.log
done
 
bash pipeline.sh &> log/run_pipeline.out
