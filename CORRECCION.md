### merge_fastqs.sh

- [-0.5] Se utilizan patrones específicos para dos archivos diferentes en lugar de sólo el sample id ($1/$sid*.fastq.gz)

### download.sh

- [-0.5] $wd no está definida, el script debería recibir la ruta completa (`$wd/res` en la llamada desde pipeline.sh)

### pipeline.sh

- [-1] El script no funciona tal cual está
- [0] Error al declarar la variable wd con `d=$(pwd)`
- [-0.5] Cambio de directorio con `cd` en lugar de usar rutas relativas
- [-1] `out/star/$sid/log.final.out` no es un archivo válido
